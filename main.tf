resource "aws_iam_user" "this" {
  name = var.name
  path = var.path
}

resource "aws_iam_user_login_profile" "this" {
    depends_on = [aws_iam_user.this]

  user                    = aws_iam_user.this.name
  pgp_key                 = var.pgp_key
  password_reset_required = var.password_reset_required
}

resource "aws_iam_user_policy_attachment" "super-user" {
  depends_on = [aws_iam_user.this]
  user       = aws_iam_user.this.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_access_key" "this" {
    depends_on = [aws_iam_user.this]



  user    = aws_iam_user.this.name
  pgp_key = var.pgp_key
}


output "aws_iam_user-credentials" {
  description = "The credentials of a given IAM user"
  value = {
    name = var.create-aws_iam_user ? aws_iam_user.this.name : null
    encrypted_password = var.create-aws_iam_user_login_profile ? aws_iam_user_login_profile.this.encrypted_password : null
    pgp_key = var.pgp_key
    access-key-id = var.create-aws_iam_access_key ? aws_iam_access_key.this.id : null
    encrypted-secret-access-key = var.create-aws_iam_access_key ? aws_iam_access_key.this.encrypted_secret : null
  }
}
